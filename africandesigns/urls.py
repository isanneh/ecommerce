from django.conf.urls import patterns, url

from africandesigns import views

urlpatterns = patterns('',
    url(r'^register/$', views.register, name = 'register'),
    url(r'^signin/$', views.signin, name = 'signin'),
    url(r'^home/$', views.home, name = 'home'),
    url(r'^logout/$', views.logout, name = 'logout'),
    url(r'^add_store/$', views.add_store, name = 'add_store'),
    url(r'^store/(?P<store_id>\d+)/$', views.store, name = 'store'),
    url(r'^add_product/$', views.add_product, name = 'add_product'),
    url(r'^product/(?P<product_id>\d+)/$', views.product, name = 'product'),
    url(r'^add_item_to_cart/(?P<product_id>\d+)/$', views.add_item_to_cart, name = 'add_item_to_cart'),
    url(r'^shopping_cart/$', views.shopping_cart, name = 'shopping_cart'),
    url(r'^total_shopping_items/$', views.total_shopping_items, name = 'total_shopping_items'),
    url(r'^edit_product/(?P<product_id>\d+)/$', views.edit_product, name = 'edit_product'),
    url(r'^edit_store/(?P<store_id>\d+)/$', views.edit_store, name = 'edit_store'),


    url(r'^page/$', views.page, name = 'page'),
    url(r'^load/$', views.load, name = 'load'),
    url(r'^shopping_cart/(?P<store_id>\d+)/(?P<product_index>\d+)/$', views.shopping_cart, name = 'shopping_cart'),
    url(r'^delete_product/(?P<product_id>\d+)/$', views.delete_product, name='delete_product'),
    url(r'^checkout/(?P<store_id>\d+)/$', views.checkout, name = 'checkout'),

    url(r'^orders/(?P<store_id>\w+)/$', views.orders, name = 'orders'),
    url(r'^orders/(?P<store_id>\w+)/(?P<order_filter>\w+)/$', views.orders, name = 'orders'),

    url(r'^order/(?P<order_id>\d+)/$', views.order, name = 'order'),
    url(r'^update_shipping/(?P<order_id>\d+)/$', views.update_shipping, name = 'update_shipping'),
    url(r'^shipping/$', views.shipping, name = 'shipping'),

    url(r'^my_orders/$', views.my_orders, name = 'my_orders'),
    url(r'^my_orders/(?P<order_filter>\w+)/$', views.my_orders, name = 'my_orders'),
    url(r'^my_order/(?P<order_id>\d+)/$', views.my_order, name = 'my_order'),

    url(r'^my_stores/$', views.my_stores, name = 'my_stores'),

    url(r'^edit_account/$', views.edit_account, name = 'edit_account'),
    url(r'^change_password/$', views.change_password, name = 'change_password'),
    url(r'^store_admin/(?P<store_id>\d+)/$', views.store_admin, name = 'store_admin'),
    url(r'^remove_admin/(?P<admin_id>\d+)/$', views.remove_admin, name = 'remove_admin'),

    url(r'^e_content/$', views.e_content, name = 'e_content'),












)

