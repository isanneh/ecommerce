# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shipping.weight_pounds'
        db.add_column(u'africandesigns_shipping', 'weight_pounds',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)

        # Adding field 'Shipping.weight_ounces'
        db.add_column(u'africandesigns_shipping', 'weight_ounces',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)

        # Adding field 'Shipping.zipcode'
        db.add_column(u'africandesigns_shipping', 'zipcode',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shipping.weight_pounds'
        db.delete_column(u'africandesigns_shipping', 'weight_pounds')

        # Deleting field 'Shipping.weight_ounces'
        db.delete_column(u'africandesigns_shipping', 'weight_ounces')

        # Deleting field 'Shipping.zipcode'
        db.delete_column(u'africandesigns_shipping', 'zipcode')


    models = {
        u'africandesigns.admins': {
            'Meta': {'object_name': 'Admins'},
            'admin_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Stores']"})
        },
        u'africandesigns.images': {
            'Meta': {'object_name': 'Images'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.orders': {
            'Meta': {'object_name': 'Orders'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'buyer_id': ('django.db.models.fields.IntegerField', [], {}),
            'city': ('django.db.models.fields.TextField', [], {}),
            'country': ('django.db.models.fields.TextField', [], {}),
            'date_ordered': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"}),
            'shipped': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.TextField', [], {}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.products': {
            'Meta': {'object_name': 'Products'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'store_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.shipping': {
            'Meta': {'object_name': 'Shipping'},
            'cost': ('django.db.models.fields.FloatField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'duration': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"}),
            'weight_ounces': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'weight_pounds': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'africandesigns.stores': {
            'Meta': {'object_name': 'Stores'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'owner_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['africandesigns']