# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User_Shipping'
        db.create_table(u'africandesigns_user_shipping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('address', self.gf('django.db.models.fields.TextField')(default='null')),
            ('city', self.gf('django.db.models.fields.TextField')(default='null')),
            ('state', self.gf('django.db.models.fields.TextField')(default='null')),
            ('zipcode', self.gf('django.db.models.fields.IntegerField')()),
            ('country', self.gf('django.db.models.fields.TextField')(default='United States')),
        ))
        db.send_create_signal(u'africandesigns', ['User_Shipping'])


    def backwards(self, orm):
        # Deleting model 'User_Shipping'
        db.delete_table(u'africandesigns_user_shipping')


    models = {
        u'africandesigns.admins': {
            'Meta': {'object_name': 'Admins'},
            'admin_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Stores']"})
        },
        u'africandesigns.images': {
            'Meta': {'object_name': 'Images'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.orders_products': {
            'Meta': {'object_name': 'Orders_Products'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Orders_Shipping']"}),
            'product_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.orders_shipping': {
            'Meta': {'object_name': 'Orders_Shipping'},
            'address': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'buyer_id': ('django.db.models.fields.IntegerField', [], {}),
            'city': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'country': ('django.db.models.fields.TextField', [], {'default': "'United States'"}),
            'date_ordered': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'shipped': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'store_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.products': {
            'Meta': {'object_name': 'Products'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'store_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.shipping': {
            'Meta': {'object_name': 'Shipping'},
            'cost': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'duration': ('django.db.models.fields.TextField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"}),
            'weight_ounces': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'weight_pounds': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'africandesigns.stores': {
            'Meta': {'object_name': 'Stores'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'owner_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'africandesigns.user_shipping': {
            'Meta': {'object_name': 'User_Shipping'},
            'address': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'city': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'country': ('django.db.models.fields.TextField', [], {'default': "'United States'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['africandesigns']