# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Orders_Shipping'
        db.create_table(u'africandesigns_orders_shipping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order_id', self.gf('django.db.models.fields.IntegerField')()),
            ('store_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('buyer_id', self.gf('django.db.models.fields.IntegerField')()),
            ('address', self.gf('django.db.models.fields.TextField')(default='null')),
            ('city', self.gf('django.db.models.fields.TextField')(default='null')),
            ('state', self.gf('django.db.models.fields.TextField')(default='null')),
            ('zipcode', self.gf('django.db.models.fields.IntegerField')()),
            ('country', self.gf('django.db.models.fields.TextField')(default='United States')),
            ('shipped', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_ordered', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Orders_Shipping'])

        # Deleting field 'Orders.address'
        db.delete_column(u'africandesigns_orders', 'address')

        # Deleting field 'Orders.city'
        db.delete_column(u'africandesigns_orders', 'city')

        # Deleting field 'Orders.country'
        db.delete_column(u'africandesigns_orders', 'country')

        # Deleting field 'Orders.date_ordered'
        db.delete_column(u'africandesigns_orders', 'date_ordered')

        # Deleting field 'Orders.zipcode'
        db.delete_column(u'africandesigns_orders', 'zipcode')

        # Deleting field 'Orders.state'
        db.delete_column(u'africandesigns_orders', 'state')

        # Deleting field 'Orders.shipped'
        db.delete_column(u'africandesigns_orders', 'shipped')

        # Deleting field 'Orders.buyer_id'
        db.delete_column(u'africandesigns_orders', 'buyer_id')


    def backwards(self, orm):
        # Deleting model 'Orders_Shipping'
        db.delete_table(u'africandesigns_orders_shipping')


        # User chose to not deal with backwards NULL issues for 'Orders.address'
        raise RuntimeError("Cannot reverse this migration. 'Orders.address' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Orders.address'
        db.add_column(u'africandesigns_orders', 'address',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Orders.city'
        raise RuntimeError("Cannot reverse this migration. 'Orders.city' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Orders.city'
        db.add_column(u'africandesigns_orders', 'city',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)

        # Adding field 'Orders.country'
        db.add_column(u'africandesigns_orders', 'country',
                      self.gf('django.db.models.fields.TextField')(default='United States'),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Orders.date_ordered'
        raise RuntimeError("Cannot reverse this migration. 'Orders.date_ordered' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Orders.date_ordered'
        db.add_column(u'africandesigns_orders', 'date_ordered',
                      self.gf('django.db.models.fields.DateTimeField')(),
                      keep_default=False)

        # Adding field 'Orders.zipcode'
        db.add_column(u'africandesigns_orders', 'zipcode',
                      self.gf('django.db.models.fields.IntegerField')(default=datetime.datetime(2013, 8, 26, 0, 0)),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Orders.state'
        raise RuntimeError("Cannot reverse this migration. 'Orders.state' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Orders.state'
        db.add_column(u'africandesigns_orders', 'state',
                      self.gf('django.db.models.fields.TextField')(),
                      keep_default=False)

        # Adding field 'Orders.shipped'
        db.add_column(u'africandesigns_orders', 'shipped',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Orders.buyer_id'
        raise RuntimeError("Cannot reverse this migration. 'Orders.buyer_id' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Orders.buyer_id'
        db.add_column(u'africandesigns_orders', 'buyer_id',
                      self.gf('django.db.models.fields.IntegerField')(),
                      keep_default=False)


    models = {
        u'africandesigns.admins': {
            'Meta': {'object_name': 'Admins'},
            'admin_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Stores']"})
        },
        u'africandesigns.images': {
            'Meta': {'object_name': 'Images'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.orders': {
            'Meta': {'object_name': 'Orders'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.orders_shipping': {
            'Meta': {'object_name': 'Orders_Shipping'},
            'address': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'buyer_id': ('django.db.models.fields.IntegerField', [], {}),
            'city': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'country': ('django.db.models.fields.TextField', [], {'default': "'United States'"}),
            'date_ordered': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_id': ('django.db.models.fields.IntegerField', [], {}),
            'shipped': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.TextField', [], {'default': "'null'"}),
            'store_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.products': {
            'Meta': {'object_name': 'Products'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'store_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.shipping': {
            'Meta': {'object_name': 'Shipping'},
            'cost': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'duration': ('django.db.models.fields.TextField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"}),
            'weight_ounces': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'weight_pounds': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'africandesigns.stores': {
            'Meta': {'object_name': 'Stores'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'owner_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['africandesigns']