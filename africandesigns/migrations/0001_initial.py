# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Stores'
        db.create_table(u'africandesigns_stores', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('owner_id', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Stores'])

        # Adding model 'Admins'
        db.create_table(u'africandesigns_admins', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['africandesigns.Stores'])),
            ('admin_id', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Admins'])

        # Adding model 'Products'
        db.create_table(u'africandesigns_products', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store_id', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Products'])

        # Adding model 'Shipping'
        db.create_table(u'africandesigns_shipping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['africandesigns.Products'])),
            ('cost', self.gf('django.db.models.fields.FloatField')()),
            ('duration', self.gf('django.db.models.fields.TextField')()),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Shipping'])

        # Adding model 'Orders'
        db.create_table(u'africandesigns_orders', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['africandesigns.Products'])),
            ('buyer_id', self.gf('django.db.models.fields.IntegerField')()),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('city', self.gf('django.db.models.fields.TextField')()),
            ('state', self.gf('django.db.models.fields.TextField')()),
            ('zipcode', self.gf('django.db.models.fields.IntegerField')()),
            ('country', self.gf('django.db.models.fields.TextField')()),
            ('shipped', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_ordered', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'africandesigns', ['Orders'])

        # Adding model 'Images'
        db.create_table(u'africandesigns_images', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['africandesigns.Products'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'africandesigns', ['Images'])


    def backwards(self, orm):
        # Deleting model 'Stores'
        db.delete_table(u'africandesigns_stores')

        # Deleting model 'Admins'
        db.delete_table(u'africandesigns_admins')

        # Deleting model 'Products'
        db.delete_table(u'africandesigns_products')

        # Deleting model 'Shipping'
        db.delete_table(u'africandesigns_shipping')

        # Deleting model 'Orders'
        db.delete_table(u'africandesigns_orders')

        # Deleting model 'Images'
        db.delete_table(u'africandesigns_images')


    models = {
        u'africandesigns.admins': {
            'Meta': {'object_name': 'Admins'},
            'admin_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Stores']"})
        },
        u'africandesigns.images': {
            'Meta': {'object_name': 'Images'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.orders': {
            'Meta': {'object_name': 'Orders'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'buyer_id': ('django.db.models.fields.IntegerField', [], {}),
            'city': ('django.db.models.fields.TextField', [], {}),
            'country': ('django.db.models.fields.TextField', [], {}),
            'date_ordered': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"}),
            'shipped': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.TextField', [], {}),
            'zipcode': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.products': {
            'Meta': {'object_name': 'Products'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'store_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'africandesigns.shipping': {
            'Meta': {'object_name': 'Shipping'},
            'cost': ('django.db.models.fields.FloatField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'duration': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['africandesigns.Products']"})
        },
        u'africandesigns.stores': {
            'Meta': {'object_name': 'Stores'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'owner_id': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['africandesigns']