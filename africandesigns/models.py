from django.db import models
from django.contrib.auth import get_user_model #django user model

# Create your models here.

class Stores(models.Model):
    """Model to hold store product information"""
    owner_id = models.IntegerField()
    name = models.TextField()
    description = models.TextField()
    status = models.IntegerField(default=0) #0 = inactive, 1=active, #2=suspended
    zipcode = models.IntegerField(default=0) #store zipcode
    date_created = models.DateTimeField()

    def owner(self):
	user = get_user_model().objects.get(id = self.owner_id)
	return user

class Admins(models.Model):
    """Other admins of store apart from owner"""
    store = models.ForeignKey(Stores)
    admin_id = models.IntegerField()

    def admin(self):
	user = get_user_model().objects.get(id = self.admin_id)
	return user

class Products(models.Model):
    """Model to hold store product information"""
    store_id = models.IntegerField()
    name = models.TextField()
    quantity = models.IntegerField()
    price = models.FloatField()
    description = models.TextField()
    date_added = models.DateTimeField()

    def store(self):
	store = Stores.objects.get(id = self.store_id)
	return store


class Shipping(models.Model):
    """Shipping cost"""
    product = models.ForeignKey(Products)
    cost = models.FloatField(default=0)
    duration = models.TextField(default=0)
    weight_pounds = models.IntegerField(default=0)
    weight_ounces = models.FloatField(default=0)
    zipcode = models.IntegerField(default=0) #zipcode product is being shipped from
    date_added = models.DateTimeField()

class Orders_Shipping(models.Model):
    """buyer's shipping information"""
    store_id = models.IntegerField(default=0)
    buyer_id = models.IntegerField()
    address = models.TextField(default='null')
    city = models.TextField(default='null')
    state = models.TextField(default='null')
    zipcode = models.IntegerField()
    country = models.TextField(default='United States')
    shipped = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    date_ordered = models.DateTimeField()

    def buyer(self):
	user = get_user_model().objects.get(id = self.buyer_id)
	return user

    def seller(self):
	store = Stores.objects.get(id = self.store_id)
	return store

class Orders_Products(models.Model):
    """Produts in Orders"""
    order = models.ForeignKey(Orders_Shipping)
    product_id = models.IntegerField()

    def get_product(self):
	product = Products.objects.get(id = self.product_id)
	return product


class Images(models.Model):
    print "hhh"
    """Model to hold images of products"""
    product = models.ForeignKey(Products)
    image = models.ImageField(upload_to = 'africandesigns/static/products')

    def image_name(self):
	""" image sub path (products/[image name]) needed to display images"""
	return (str(self.image.url.split('/')[2]) + '/' + str(self.image.url.split('/')[3]))

class User_Shipping(models.Model):
    """user's shipping information"""
    user_id = models.IntegerField(default=0)
    address = models.TextField(default='null')
    city = models.TextField(default='null')
    state = models.TextField(default='null')
    zipcode = models.IntegerField()
    country = models.TextField(default='United States')

    def get_user(self):
	user = get_user_model().objects.get(id = self.user_id)
	return user





    

