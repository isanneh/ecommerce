# Create your views here.

from django.http import HttpResponse

from django.shortcuts import render
from django.contrib.auth.models import User

from django.contrib.auth import get_user_model #django user model

from africandesigns.forms import * #file where forms are located

from africandesigns.models import * #database

from django.contrib import messages #flash messages
from django.shortcuts import redirect

from django.http import HttpResponseRedirect

from django.contrib.auth import authenticate, login, logout

from datetime import datetime # date library

from django.shortcuts import get_object_or_404 #404 error if the data requested is not in the database

from django.core.urlresolvers import reverse

from django.core.exceptions import ObjectDoesNotExist # exception



"""  """
def page(request):
    return render(request, 'africandesigns/page.html')

def e_content(request):
    return render(request, 'africandesigns/e_content.html')


"""  """
def load(request):
    return render(request, 'africandesigns/load.html')


""" home """
def home(request):
    products = Products.objects.all()
    return render(request, 'africandesigns/home.html', {'products': products,})

""" user registration """
def register(request):
    if request.method == 'POST': #if the form has been submitted
	form = RegisterForm(request.POST)
	if form.is_valid(): #form is validated
	    # add user to database

	    user = get_user_model().objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])

	    user.first_name = form.cleaned_data['first_name']
	    user.last_name = form.cleaned_data['last_name']
	    user.is_active = True
	    user.save()

	    messages.add_message(request, messages.INFO, "Registration successful!")
	    #render(request, 'africandesigns/signin.html')
	    return HttpResponseRedirect(reverse('africandesigns:signin'))

    else:
        form = RegisterForm()
    return render(request, 'africandesigns/register.html',  {'form': form, 'title': 'Register'})

""" user signin """
def signin(request):
    if request.method == 'POST':
	form = SigninForm(request.POST)
	if form.is_valid():
	    # check email and password match
	    query_user = get_user_model().objects.filter(email = form.cleaned_data['email'])
	    print "len", len(query_user)
	    #query_user[0].delete()
	    #query_user[0].save()
	    if len(query_user) == 1: 
		user = query_user[0]
		if user.check_password(form.cleaned_data['password']) == True: # authentication successful
		    if user.is_active: # account is active
		        messages.add_message(request, messages.INFO, "You have successfully signed in!")
		        #store user in session to indicate user is logged in
		        request.session['user'] = user
			#login
			#login(request, user)
    		        return HttpResponseRedirect(reverse('africandesigns:home'))
		    else: # account is inactive
	    	        messages.add_message(request, messages.INFO, "Sorry, you cannot login because you account has been deactivated!")
    			return render(request, 'africandesigns/signin.html', {'form': form, 'title': 'Signin'}) 	
	        else: # account is inactive
	            messages.add_message(request, messages.INFO, "Sorry, email address and/or password is incorrect!")
    		    return render(request, 'africandesigns/signin.html', {'form': form, 'title': 'Signin'})

    else:
	form = SigninForm()
    return render(request, 'africandesigns/signin.html', {'form': form, 'title': 'Signin'})

""" Logout user """ 
def logout(request):
    #remove user from session to logout user if user is currently logged in
    if 'user' in request.session:
	del request.session['user']
	messages.add_message(request, messages.INFO, "You have successfully logged out!")
    return HttpResponseRedirect(reverse('africandesigns:signin'))


""" add store """
def add_store(request):
    if request.method == 'POST': #if the form has been submitted
	form = AddStoreForm(request.POST)
	if form.is_valid(): #form is validated
	    # add store to database
	    store = Stores(owner_id = request.session['user'].id, name = form.cleaned_data['name'], description = form.cleaned_data['description'], status = form.cleaned_data['status'], zipcode = form.cleaned_data['zipcode'], date_created = datetime.now())

	    store.save()

	    messages.add_message(request, messages.INFO, "Store successfully added!")
	    return HttpResponseRedirect(reverse('africandesigns:store',  kwargs={'store_id': store.id})) 
 
    else:
        form = AddStoreForm()
    return render(request, 'africandesigns/add_store.html',  {'form': form, 'title': 'Add Store'})

""" store """
def store(request, store_id):
    #return HttpResponse("Hello, world. You're at the store.")
    store = Stores.objects.get(id = store_id)
    #add store to session
    request.session['store'] = store
    #store = Stores.objects.all()
    print store.name
    products = Products.objects.filter(store_id = store_id)


    #check if user is logged in
    if 'user' in request.session:

        try:
	    admin = Admins.objects.get(admin_id = request.session['user'].id)
	    is_admin = True
        except ObjectDoesNotExist:
	    is_admin = False

    else:
	is_admin = False

    return render(request, 'africandesigns/store.html',  {'title': 'Store', 'store': store, 'products': products, 'is_admin': is_admin,})

    """ s = Products.objects.all()
    for a in s:
	a.delete()
	print "v" """


import os


""" add product """
def add_product(request):
    if request.method == 'POST': #if the form has been submitted
	form = AddProductForm(request.POST)
	if form.is_valid(): #form is validated
	    # add product to database
	    product= Products(store_id = request.session['store'].id, name = form.cleaned_data['name'], description = form.cleaned_data['description'], quantity = form.cleaned_data['quantity'], price = form.cleaned_data['price'], date_added = datetime.now())

	    product.save()

	    if form.cleaned_data['ounces'] == '':
		ounces = 0
	    else:
		ounces = form.cleaned_data['ounces']

	    shipping = product.shipping_set.create(weight_pounds = form.cleaned_data['pounds'], weight_ounces = ounces, zipcode = form.cleaned_data['zipcode'], date_added = datetime.now())

	    shipping.save()

	    if('image1' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image1'])
	    	image.save()
		#save image filename as productid_[product id number]_imageid[image id number]
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		
		print "1"
	    if('image2' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image2'])
	    	image.save()
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		print "2"
	    if('image3' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image3'])
	    	image.save()
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		print "3"

	    product.save()

	    print "yes"
	    messages.add_message(request, messages.INFO, "Product successfully added!")
	    return HttpResponseRedirect(reverse('africandesigns:product', kwargs={'product_id': product.id})) 

            #print "bbb", os.path.splitext(request.FILES['image1'].name)[0], os.path.splitext(request.FILES['image1'].name)  # Will return just 'file'
            #return HttpResponseRedirect(reverse('africandesigns:product', kwargs={'product_id': 1})) 

        else:
	    print "invalid entry!"
    else:
	data = {
	    'zipcode': request.session['store'].zipcode,
	}
        form = AddProductForm()
        return render(request, 'africandesigns/add_product.html',  {'form': form, 'title': 'Add Product'})





""" product """
def product(request, product_id):
    product = Products.objects.get(id = product_id)
    return render(request, 'africandesigns/product.html',  {'title': 'Product', 'product': product})


""" shopping cart: each store has a separate shopping cart """
def add_item_to_cart(request, product_id):
    product = Products.objects.get(id = product_id)
    #check if shopping cart is not empty. request.session['shopping-cart'] contains a list of stores whose products are currently added to the shopping cart
    if 'shopping_cart' in request.session:
	print 1
	#check if the store's shopping cart is not empty
        if str(product.store_id) in request.session['shopping_cart']:
	    #current products in store's shopping carts
	    current_products = request.session['shopping_cart'][str(product.store_id)]
	    print "cur1", current_products, request.session['shopping_cart'][str(product.store_id)]
	    #add new product to store's shopping cart
	    current_products.append(product)
	    #print "cu2r", current_products
	    request.session['shopping_cart'][str(product.store_id)] = current_products
	    print 2, request.session['shopping_cart'][str(product.store_id)], current_products, 't'
	    request.session.modified = True

	    #print "curr", current_products
	    #request.session['shopping_cart'][str(product.store_id)].append(product)

	    #print 'b', request.session['shopping_cart'][str(product.store_id)], 't'
	#otherwise, store's shopping cart is empty
	else:
	    #add product to store's shopping cart 
	    request.session['shopping_cart'][str(product.store_id)] = [product]
	    request.session.modified = True
	    print 3
	    
	    
    #otherwise, shopping cart is empty
    else:
	#add store to shopping cart session, and add new product to store
	request.session['shopping_cart'] = {str(product.store_id): [product]}
	print 4

    return HttpResponseRedirect(reverse('africandesigns:shopping_cart')) 

""" shopping cart """
def shopping_cart(request,store_id=None, product_index=None):
    if store_id and product_index:
	print "yes", store_id, product_index
        #delete product from shopping cart store session
        current_products = request.session['shopping_cart'][str(store_id)]
        current_products.pop(int(product_index))
        request.session['shopping_cart'][str(store_id)] = current_products
        if len(current_products) == 0:
	    del request.session['shopping_cart'][str(store_id)]
        request.session.modified = True
    return render(request, 'africandesigns/shopping_cart.html')

""" shopping cart """
def shopping_cart2(request,store_id, product_index):
    #delete product from shopping cart store session
    current_products = request.session['shopping_cart'][str(store_id)]
    current_products.pop(int(product_index))
    request.session['shopping_cart'][str(store_id)] = current_products
    if len(current_products) == 0:
	del request.session['shopping_cart'][str(store_id)]
    request.session.modified = True
    return render(request, 'africandesigns/shopping_cart.html')

#total number of items in shopping cart 
def total_shopping_items(request):
    #check if shopping cart is not empty. request.session['shopping-cart'] contains a list of stores whose products are currently added to the shopping cart
    if 'shopping_cart' in request.session:
	#check if there are no stores in the shopping cart session
	if(len(request.session['shopping_cart']) == 0):
	    items = 0
	#count the total number of products available in the shopping cart
	else:
	    items = 0
	    shopping_cart = request.session['shopping_cart']
	    store_keys = shopping_cart.keys()
	    for key in store_keys: 
		items = items + len(shopping_cart[key])
    else:
	items = 0
    return render(request, 'africandesigns/total_shopping_items.html',  {'items': items})



""" shopping cart 
def shopping_cart(request):
    #check if shopping cart is not empty. request.session['shopping-cart'] contains a list of stores whose products are currently added to the shopping cart
    if 'shopping_cart' in request.session:
	#check if there are no stores in the shopping cart session
	if(len(request.session['shopping_cart']) == 0):
	    items = 0
	#count the total number of products available in the shopping cart
	else:
	    items = 0
	    shopping_cart = request.session['shopping_cart']
	    store_keys = shopping_cart.keys()
	    for key in store_keys: 
		items = items + len(shopping_cart[key])
    else:
	items = 0
    return render(request, 'africandesigns/shopping_cart.html',  {'items': items})"""


""" edit product """
def edit_product(request, product_id):

    if request.method == 'POST': #if the form has been submitted
	form = AddProductForm(request.POST)
	if form.is_valid(): #form is validated
	    # edit product in database
	    product = Products.objects.get(id = product_id)
	    product.name = form.cleaned_data['name']
	    product.description = form.cleaned_data['description']
	    product.quantity = form.cleaned_data['quantity']
	    product.price = form.cleaned_data['price']

	    product.save()

	    product = Products.objects.get(id = product_id)

	    if form.cleaned_data['ounces'] == '':
		ounces = 0
	    else:
		ounces = form.cleaned_data['ounces']

	    shipping = product.shipping_set.all()[0]
	    shipping.weight_pounds = form.cleaned_data['pounds']
	    shipping.weight_ounces = ounces
	    shipping.zipcode = form.cleaned_data['zipcode']

	    shipping.save()


	    '''shipping = product.shipping_set.create(weight_pounds = form.cleaned_data['pounds'], weight_ounces = ounces, zipcode = form.cleaned_data['zipcode'], date_added = datetime.now())

	    shipping.save()'''


	    '''if('image1' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image1'])
	    	image.save()
		#save image filename as productid_[product id number]_imageid[image id number]
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		
		print "1"
	    if('image2' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image2'])
	    	image.save()
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		print "2"
	    if('image3' in request.FILES):
	        image = product.images_set.create(image = request.FILES['image3'])
	    	image.save()
		#image.image.name = 'productid_' + str(product.id) + '_imageid_' + str(image.id)
    	        #image.save()
		print "3"

	    product.save()'''

	    messages.add_message(request, messages.INFO, "Product successfully edited!")
	    return HttpResponseRedirect(reverse('africandesigns:product', kwargs={'product_id': product.id})) 

            #print "bbb", os.path.splitext(request.FILES['image1'].name)[0], os.path.splitext(request.FILES['image1'].name)  # Will return just 'file'
            #return HttpResponseRedirect(reverse('africandesigns:product', kwargs={'product_id': 1})) 
 
    else:
	product = Products.objects.get(id = product_id)
	shipping = product.shipping_set.all()[0]
	data = {
	        'name': product.name,
		'description': product.description,
		'price': product.price,
		'quantity': product.quantity,
		'pounds': shipping.weight_pounds,
		'ounces': shipping.weight_ounces,
		'zipcode': shipping.zipcode,
	}
        form = AddProductForm(data)
        return render(request, 'africandesigns/edit_product.html',  {'form': form, 'title': 'Edit Product', 'product_id': product.id})


""" edit product """
def edit_store(request, store_id):
    if request.method == 'POST': #if the form has been submitted
	form = AddStoreForm(request.POST)
	store = Stores.objects.get(id = store_id)
	if form.is_valid(): #form is validated
	    # edit store in database
	    store.name = form.cleaned_data['name']
	    store.description = form.cleaned_data['description']
	    store.status = form.cleaned_data['status']
	    store.zipcode = form.cleaned_data['zipcode']

	    store.save()

	    messages.add_message(request, messages.INFO, "Store successfully edited!")
	    return HttpResponseRedirect(reverse('africandesigns:store', kwargs={'store_id': store.id})) 
 
    else:
	store = Stores.objects.get(id = store_id)
	data = {
	        'name': store.name,
		'description': store.description,
		'status': store.status,
		'zipcode': store.zipcode
	}
        form = AddStoreForm(data)
        return render(request, 'africandesigns/edit_store.html',  {'form': form, 'title': 'Edit Store', 'store_id': store.id})


def delete_product(request, product_id):
    """ delete product """
    product = Products.objects.get(id = product_id)
    #product = Products.objects.filter(id = product_id)[0]
    store_id = product.store_id
    print product.name
    product.delete()
    #product.save()
    print product.name
    print product
    messages.add_message(request, messages.INFO, "Product successfully deleted!")
    return HttpResponseRedirect(reverse('africandesigns:store', kwargs={'store_id': store_id})) 


def checkout(request, store_id):
    """ checkout """
    if request.method == 'POST':
        form = ShippingForm(request.POST)
	orders = Orders_Shipping.objects.all()
	'''for order in orders:
	    order.delete()'''
	if form.is_valid(): 
	    #add order information to database if validation is successful
	    order = Orders_Shipping(store_id = store_id, buyer_id = request.session['user'].id, address = form.cleaned_data['address'], city =  form.cleaned_data['city'], state = form.cleaned_data['state'], zipcode = form.cleaned_data['zipcode'], country = 'United States', date_ordered = datetime.now())
	    order.save() 
	    print "yes", order.orders_products_set.count()
	    #add store's products in shopping cart session to database
	    for product in request.session['shopping_cart'][str(store_id)]:
		order.orders_products_set.create(product_id = product.id)
    	    return render(request, 'africandesigns/payment.html',  {'title': 'Payment', 'order_id': order.id})
	else:
    	    return render(request, 'africandesigns/checkout.html',  {'form': form, 'title': 'invalid', 'store_id': store_id})
    else:
	print "no"
        form = ShippingForm()
        return render(request, 'africandesigns/checkout.html',  {'form': form, 'title': 'Checkout', 'store_id': store_id})


def orders(request, store_id, order_filter = 'all'):
    """ orders """
    #check if pending orders option is selected
    if order_filter == 'pending':
        orders = Orders_Shipping.objects.filter(shipped = False, store_id = store_id)
    #check if fulfilled orders option is selected
    elif order_filter == 'fulfilled':
        orders = Orders_Shipping.objects.filter(shipped = True, store_id = store_id)
    #return all orders
    else: 
        orders = Orders_Shipping.objects.filter(store_id = store_id)
    return render(request, 'africandesigns/orders.html', {'title': 'Orders', 'orders': orders, 'store_id': store_id})

def order(request, order_id):
    """ order """
    order = Orders_Shipping.objects.get(id = order_id)
    return render(request, 'africandesigns/order.html', {'title': 'Order', 'order': order})

def update_shipping(request, order_id):
    """ update shipping """
    order = Orders_Shipping.objects.get(id = order_id)
    order.shipped = True
    order.save()
    messages.add_message(request, messages.INFO, "Shipping status has been updated!")
    return render(request, 'africandesigns/order.html', {'title': 'Order', 'order': order})


def shipping(request):
    """ user shipping info """
    if request.method == 'POST':
        form = ShippingForm(request.POST)
	if form.is_valid(): 
	    #check if user already added shipping info
	    try:
	        shipping = User_Shipping.objects.get(user_id = request.session['user'].id)
		shipping.user_id = request.session['user'].id
		shipping.address = form.cleaned_data['address']
		shipping.city = form.cleaned_data['city']
		shipping.state = form.cleaned_data['state']
		shipping.zipcode = form.cleaned_data['zipcode']
		shipping.save()
	    except ObjectDoesNotExist:
	        #add user shipping information to database if validation is successful
	        shipping = User_Shipping(user_id = request.session['user'].id, address = form.cleaned_data['address'], city =  form.cleaned_data['city'], state = form.cleaned_data['state'], zipcode = form.cleaned_data['zipcode'], country = 'United States')
	        shipping.save() 

    	    return HttpResponse("Shipping address added!")
	else:
    	    return render(request, 'africandesigns/shipping.html',  {'form': form, 'title': 'invalid'})
    else:
        try:
            shipping = User_Shipping.objects.get(user_id = request.session['user'].id)
	    data = {
		'address': shipping.address,
		'city': shipping.city,
		'state': shipping.state,
		'zipcode': shipping.zipcode,
		'country': shipping.country,
	    }
            form = ShippingForm(data)
	except ObjectDoesNotExist:
            form = ShippingForm()
        return render(request, 'africandesigns/shipping.html',  {'form': form, 'title': 'shipping'})


def my_orders(request, order_filter = 'all'):
    """ user's orders """
    #check if pending orders option is selected
    if order_filter == 'pending':
        orders = Orders_Shipping.objects.filter(shipped = False, buyer_id = request.session['user'].id)
    #check if fulfilled orders option is selected
    elif order_filter == 'fulfilled':
        orders = Orders_Shipping.objects.filter(shipped = True, buyer_id = request.session['user'].id)
    #return all orders
    else: 
        orders = Orders_Shipping.objects.filter( buyer_id = request.session['user'].id)
    return render(request, 'africandesigns/my_orders.html', {'title': 'Orders', 'orders': orders})

def my_order(request, order_id):
    """ user's order """
    order = Orders_Shipping.objects.get(id = order_id)
    return render(request, 'africandesigns/my_order.html', {'title': 'Order', 'order': order})


""" my stores """
def my_stores(request):
    stores = Stores.objects.filter(owner_id = request.session['user'].id)
    admin_stores = Admins.objects.filter(admin_id = request.session['user'].id)
    return render(request, 'africandesigns/my_stores.html', {'stores': stores, 'admin_stores': admin_stores})


""" edit account """
def edit_account(request):
    user = get_user_model().objects.get(id = request.session['user'].id)
    if request.method == 'POST': #if the form has been submitted
	form = AccountForm(request.POST)
	if form.is_valid(): #form is validated
	    # edit user details
	    user.first_name = form.cleaned_data['first_name']
	    user.last_name = form.cleaned_data['last_name']
	    user.is_active = True
	    user.save()

	    messages.add_message(request, messages.INFO, "Account successfully edited!")
	    return HttpResponseRedirect(reverse('africandesigns:home')) 

    else:
	data = {
	'first_name': user.first_name,
	'last_name': user.last_name,
	'username': user.username,
	'email': user.email,
	}
        form = AccountForm(data)
    return render(request, 'africandesigns/edit_account.html',  {'form': form, 'title': 'Edit Account'})


""" change_password """
def change_password(request):
    user = get_user_model().objects.get(id = request.session['user'].id)
    if request.method == 'POST': #if the form has been submitted
	form = PasswordForm(request.POST)
	if form.is_valid(): #form is validated
	    #checks if current password is correct
	    if user.check_password(form.cleaned_data['current_password']) == True: 
		#check if new password and confirm new password are the same
		if form.cleaned_data['new_password'] == form.cleaned_data['confirm_password']: 
		    # change password
		    user.set_password(form.cleaned_data['new_password'])
		    #user.password = form.cleaned_data['new_password']
		    user.save()
	    	    messages.add_message(request, messages.INFO, "Password successfully changed!")
	    	    return HttpResponseRedirect(reverse('africandesigns:home')) 
	    	else:
		    messages.add_message(request, messages.INFO, "Passwords do not match!")
	    else:
	        messages.add_message(request, messages.INFO, "Current password is incorrect!")

    else:
        form = PasswordForm()
    return render(request, 'africandesigns/change_password.html',  {'form': form, 'title': 'Change Password'})


""" store_admin """
def store_admin(request, store_id):
    store = Stores.objects.get(id = store_id)
    if request.method == 'POST': #if the form has been submitted
	form = AdminForm(request.POST)
	if form.is_valid(): #form is validated
	    #add store admin
    	    user = get_user_model().objects.get(email = form.cleaned_data['email'])
	    store.admins_set.create(admin_id = user.id)
	    messages.add_message(request, messages.INFO, "Admin successfully added!")
	    """ is_admin checks if email and confirm email address fields are the same.  
		if they are the same, it also checks if email address is registered on 
		the site, and also checks if the email address owner is already an admin 
		of the specified store """
	    '''if not form.clean(): 
		#add store admin
    		user = get_user_model().objects.get(email = form.cleaned_data['email'])
		store.admins_set.create(admin_id = user.id)
		messages.add_message(request, messages.INFO, "Admin successfully added!")
	    	#return HttpResponseRedirect(reverse('africandesigns:store_admin', kwargs={'store_id': store_id})) '''

    else:
	data = {
	'store_id': store.id,
	}
        form = AdminForm(data)
    return render(request, 'africandesigns/store_admin.html',  {'form': form, 'title': 'Add Admin', 'store': store,})

def remove_admin(request, admin_id):
    """ remove admin """
    admin = Admins.objects.get(id = admin_id)
    admin.delete()
    messages.add_message(request, messages.INFO, "Admin successfully removed!")
    return HttpResponseRedirect(reverse('africandesigns:store_admin', kwargs={'store_id': admin.store.id})) 




























