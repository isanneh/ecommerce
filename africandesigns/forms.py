
from django import forms
from django.core import validators #https://docs.djangoproject.com/en/dev/ref/forms/validation/
from africandesigns.models import * #database
from django.contrib.auth import get_user_model #django user model
from django.core.exceptions import ObjectDoesNotExist # exception


class RegisterForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

class SigninForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())

class AddStoreForm(forms.Form):
    name = forms.CharField()
    description = forms.CharField(widget=forms.Textarea())
    zipcode = forms.IntegerField(widget=forms.TextInput(attrs={'maxlength':5, 'size':5})) 
    status = forms.ChoiceField(widget = forms.Select(), choices = ([(0, 'Inactive'), (1, 'Active')]))

class AddProductForm(forms.Form):
    name = forms.CharField()
    quantity = forms.IntegerField()
    price = forms.FloatField()
    description = forms.CharField(widget=forms.Textarea())
    pounds = forms.IntegerField()
    ounces = forms.FloatField()
    zipcode = forms.IntegerField(widget=forms.TextInput(attrs={'maxlength':5, 'size':5})) 

class ShippingForm(forms.Form):
    address = forms.CharField()
    city = forms.CharField()
    state = forms.ChoiceField(widget = forms.Select(), choices = ([('New York', 'New York'), ('New Jersey', 'New Jersey')]))
    zipcode = forms.IntegerField()

class AccountForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.EmailField()

class PasswordForm(forms.Form):
    current_password = forms.CharField(widget=forms.PasswordInput())
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

class AdminForm(forms.Form):
    """ Store Admin Form"""
    email = forms.EmailField()
    confirm_email = forms.EmailField()
    store_id = forms.CharField(widget=forms.HiddenInput())

    def emailExists(self):
	""" checks if email address is registered on web site """
	cleaned_data = super(AdminForm, self).clean()
	try:
	    user = get_user_model().objects.get(email = cleaned_data.get('email'))
	    return True
	except ObjectDoesNotExist:
	    raise forms.ValidationError("Email Address owner is not a member of this site!")
	    return False


    def emailsEqual(self):
	""" checks if email and confirm_email are the same """ 
	cleaned_data = super(AdminForm, self).clean()
	if cleaned_data.get('email')  == cleaned_data.get('confirm_email'):
	    return True
	else:
	    raise forms.ValidationError("The Email Addresses are not the same!")
	    return False
	    #self._errors["cc_myself"] = self.error_class([msg])

    def checkEmail(self):
	""" checks if email and confirm_email are the same.  If they are the same, it checks if the email is a registered on the site"""
	if self.emailsEqual():
	    if self.emailExists():
		return True
	    else:
		return False
	else:
	    return False


    def clean(self): #is_admin
	""" checks email address owner is already an admin """ 
	cleaned_data = super(AdminForm, self).clean()
	#check if there are no currently no errors 
 	if len(self._errors) == 0:
	    #email = cleaned_data.get('email')
	    email = cleaned_data.get('email')
	    user = get_user_model().objects.get(email = email)
	    if self.checkEmail():
		#check if email address is the owner of the store
		if email  == Stores.objects.get(id = cleaned_data.get('store_id')).owner().email:
		    raise forms.ValidationError("Email Address owner is already an admin for this store!")
		else:
		    #check if email address is already a store admin
		    try:
			admin = Admins.objects.get(admin_id = user.id)
			raise forms.ValidationError("Email Address owner is already an admin for this store!")	

		    except ObjectDoesNotExist:
			print 2
	    else:
		print 1
        return cleaned_data

    '''def clean_recipients(self):
        data = self.cleaned_data['recipients']
        if "fred@example.com" not in data:
            raise forms.ValidationError("You have forgotten about Fred!")

        # Always return the cleaned data, whether you have changed it or
        # not.
        return data'''






