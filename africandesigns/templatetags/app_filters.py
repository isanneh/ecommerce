from django import template #https://docs.djangoproject.com/en/dev/howto/custom-template-tags/#writing-custom-template-filters

register = template.Library()

#http://stackoverflow.com/questions/8000022/django-template-how-to-lookup-a-dictionary-value-with-a-variable
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

#register.filter('get_item', get_item)

@register.filter
def get_item3(dictionary, key):
    return dictionary.get(key)

@register.filter
def get_list_index(listt, index):
    print "list", listt, len(listt), index
    return listt[index]

@register.inclusion_tag('africandesigns/total_shopping_items.html', takes_context=True)
def total_shopping_items2(context):
    request = context['request']
    #check if shopping cart is not empty. request.session['shopping-cart'] contains a list of stores whose products are currently added to the shopping cart
    if 'shopping_cart' in request.session:
	#check if there are no stores in the shopping cart session
	if(len(request.session['shopping_cart']) == 0):
	    items = 0
	#count the total number of products available in the shopping cart
	else:
	    items = 0
	    shopping_cart = request.session['shopping_cart']
	    store_keys = shopping_cart.keys()
	    for key in store_keys: 
		items = items + len(shopping_cart[key])
    else:
	items = 0
    return {
        'items': items,
    }

register.inclusion_tag('africandesigns/total_shopping_items.html', takes_context=True)(total_shopping_items2)

'''
#https://docs.djangoproject.com/en/1.3/howto/custom-template-tags/#inclusion-tags

# The first argument *must* be called "context" here.
def jump_link(context):
    return {
        'link': context['home_link'],
        'title': context['home_title'],
    }
# Register the custom tag as an inclusion tag with takes_context=True.
register.inclusion_tag('link.html', takes_context=True)(jump_link)

#http://stackoverflow.com/questions/2160261/access-request-in-django-custom-template-tags

@register.inclusion_tag('new/userinfo.html', takes_context = True)
def address(context):
    request = context['request']
    address = request.session['address']
    return {'address':address}
'''


@register.filter 
def total(listt):
    """ filter to return the total cost of the items in a shopping cart for a particular store """
    total_cost = 0
    for item in listt:
	total_cost = total_cost + float(item.price)
    return total_cost













